$( document ).ready(function() {
    initActions();
    initEncoder();
    initDecoder();
});

function initActions() {
  // Initialize the Action buttons
  $('#showEncoder').button().click(function(){
      $('#decoder').hide();
      $('#encoder').fadeIn();
  });
  $('#showDecoder').button().click(function(){
      $('#encoder').hide();
      $('#resetShares').click();
      $('#decoder').fadeIn();
  });
}

function initEncoder() {
  // Don't really submit the form on click
  $('#encode_form').submit(function(){
      return false;
  });

  // Custom method for validating thresholds
  $.validator.addMethod("threshold_lessthan_shares", function(value, element) {
      return $('#requested_threshold').val() > 1 &&
             parseInt($('#requested_threshold').val()) < parseInt($('#requested_shares').val());
  });

  // Make the execute button a button and bind it to a function
  $('#encodeExecute').button().click( function() {

      // Define our validator
      $("#encode_form").validate({
        rules: {
          plaintext: {
            required: true,
            minlength: 1,
            maxlength: 128
          },
          requested_shares: {
            required: true,
            range: [2, 20]
          },
          requested_threshold: {
            required: true,
            threshold_lessthan_shares: true
          }
        },
        messages: {
          plaintext: "Enter any text up to 128 characters",
          requested_shares: "Enter any number between 2 and 20",
          requested_threshold: "Number must be greater than 1 and less than the number of shares"
        }
      });

      // If everything is valid, then encode
      if($("#encode_form").valid()) {
        encode();
      }
  });
}


function initDecoder() {
  // Don't really submit the form on click
  $('#decode_form').submit(function(){
      return false;
  });

  // Bind the "add more shares" to a function
  $('#addShares').button().click( function() {
    var num_shares = parseInt($('#num_input_shares_shown').val());
    $('#num_input_shares_shown').val(++num_shares);
    $('#input_shares').append('<label>Share: </label> <input type="text" id="share' + num_shares +'" size="177" autocomplete="off"/><br>');
  });

  // Bind the "reset shares" to a function
  $('#resetShares').button().click( function() {
    $('#num_input_shares_shown').val("0");
    $('#input_shares > ').remove();
    $('#addShares').click();
    $('#addShares').click();
    $('#decoded_output').hide();
  });

  // Bind the "Decode and Show Plaintext" button to a function
  $('#decodeExecute').button().click(decode);
}


function decode() {
  // Grab all the shares from the input
  var inputShares = [];
  for(var i = 1; i <= parseInt($('#num_input_shares_shown').val()); i++) {
    var share = $('#share'+i).val().replace(/\s+/g, '');
    if(share.length > 1) {
      inputShares.push(share);
    }
  }

  // Parse shares into correct format
  try {
    var unwrappedShares = inputShares.map(function(s){return $.base64.atob(s);});

    // Package the shares up
    var packagedShares = [];
    for(var i = 0; i < unwrappedShares.length; i++) {
      var share = unwrappedShares[i].split(',');
      var decodedShares = [];
      for(var j = 1; j < share.length; j++) {
        decodedShares.push([parseInt(share[0]), parseInt(share[j], 36)]);
      }
      packagedShares.push(decodedShares);
    }

    // Try to decode secret
    var result = SecretSharing.solve(packagedShares, packagedShares.length, 16);

    // If we actually got something
    if(result != 0) {
      var secret = '';
      for(var i = 0; i < result.length; i += 2) {
        secret += String.fromCharCode(parseInt(result.substr(i,2),16));
      }
    }

    $('#decoded_output > p').remove();
    $('#decoded_output').append('<p>' + secret + '</p>');
    $('#decoded_output').show();


  } catch(e) {
    $('#decoded_output > p').remove();
    $('#decoded_output').append('<p class="error">' + e + '</p>');
    $('#decoded_output').show();
  }

}


function encode() {
  // Grab all the input
  var plaintext = $('#plaintext').val();
  var requested_shares = $('#requested_shares').val();
  var requested_threshold = $('#requested_threshold').val();

  // We convert the plaintext to a number so we can split it.
  var plaintext16 = "0x" + strToHex(plaintext);

  // Split it
  var splitData = SecretSharing.generate(plaintext16, requested_shares, requested_threshold);

  // Populate output
  try {
    // Clear output
    $('#encoded_output > p').remove();

    // For each share, convert the numerical arrays to a base64 encoded string
    for(var i = 0; i < splitData['shares'].length; i++) {
      var share = splitData['shares'][i];
      share.unshift(Array(i+1,i+1));  // prepend share array with share number
      var encodedShare = convertShareToBase64String(share);

      // Append to the output
      $('#encoded_output').append('<p class="share">' + encodedShare + '</p>');
    }
    $('#encoded_output').show();
  } catch(e) {
    $('#encoded_output > p').remove();
    $('#encoded_output').append('<p class="error">' + e + '</p>');
    $('#encoded_output').show();
  }
}

function strToHex(s) {
    var hex = '';
    for(var i=0; i<s.length; i++) {
        hex += '' + s.charCodeAt(i).toString(16);
    }
    return hex;
}

function convertShareToBase64String(share) {
  var b36Share = share.map(function(pair){ return pair[1].toString(36);});
  return $.base64.btoa(b36Share.join());
}
